---
title: "PBR Grenades Pack"
date: "2020-04-26"
---

## Free PBR Grenades Pack

![Grenades Pack](./grenades.jpg)

The asset pack contains the following items listed below.
### Contents
1. Grenade
2. Smoke Grenade
3. Flashbang